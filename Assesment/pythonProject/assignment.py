import pandas as pd
import json
from datetime import datetime, timedelta

file_path = r'C:\Users\Ekaterine\Desktop\DE Assessment\data.csv'
data = pd.read_csv(file_path)


def parse_contracts(contracts):
    """
    Parse a JSON formatted string.

    This function takes a single input parameter contracts, which is a JSON-formatted string.
    The function attempts to parse this string.
    If the input is NaN, not a valid JSON string, or if the parsed result is not a list
    the function returns an empty list.

    Parameters:
    contracts (str): A JSON-formatted string.

    Returns:
    list: A list of contracts if the input is a valid JSON string, otherwise an empty list.
    """
    if pd.isna(contracts):
        return []
    try:
        parsed_contracts = json.loads(contracts)
        if isinstance(parsed_contracts, list):
            return parsed_contracts
    except json.JSONDecodeError:
        pass
    return []


def get_features(row):
    """
    Calculate features based on contract data.

    1. tot_claim_cnt_l180d: Total number of claims within the last 180 days.
    2. disb_bank_loan_wo_tbc: Total disbursed bank loan amount from banks other than TBC.
    3. day_sinlastloan: Days since the last loan was taken.

    Parameters:
    row : A pandas Series containing at least the following keys:
         - contract_date, bank, summa, application_date.

    Returns:
    pd.Series: A pandas Series with the following keys:
        - tot_claim_cnt_l180d, disb_bank_loan_wo_tbc, day_sinlastloan.
    """
    contracts = parse_contracts(row['contracts'])
    application_date = datetime.fromisoformat(row['application_date'].split('+')[0])

    tot_claim_cnt_l180d = 0
    tot_claim_cnt = 0
    disb_bank_loan_wo_tbc = 0
    last_loan_date = None

    banks_to_exclude = {'LIZ', 'LOM', 'MKO', 'SUG', None}
    current_date = datetime.now()

    for contract in contracts:
        claim_date_str = contract.get('claim_date', '')
        contract_date_str = contract.get('contract_date', '')
        bank = contract.get('bank')
        loan_summa = contract.get('loan_summa', ' ')

        # Feature 1
        if claim_date_str:
            claim_date = datetime.strptime(claim_date_str, '%d.%m.%Y')
            tot_claim_cnt += 1
            if current_date - claim_date <= timedelta(days=180):
                tot_claim_cnt_l180d += 1

        # Feature 2
        if contract_date_str and bank not in banks_to_exclude:
            loan_summa = int(loan_summa) if loan_summa else 0
            disb_bank_loan_wo_tbc += loan_summa

        # Feature 3
        if contract_date_str and contract.get('summa', ' '):
            contract_date = datetime.strptime(contract_date_str, '%d.%m.%Y')
            if last_loan_date is None or contract_date > last_loan_date:
                last_loan_date = contract_date

    # Loans with summa = 0 will be included in the final set
    day_sinlastloan = (application_date - last_loan_date).days if last_loan_date else None

    if last_loan_date is None:
        disb_bank_loan_wo_tbc = -1
        day_sinlastloan = -1
    elif tot_claim_cnt == 0:
        disb_bank_loan_wo_tbc = -3
        day_sinlastloan = -3

    return pd.Series({
        'tot_claim_cnt_l180d': tot_claim_cnt_l180d if tot_claim_cnt_l180d > 0 else -3,
        'disb_bank_loan_wo_tbc': disb_bank_loan_wo_tbc,
        'day_sinlastloan': day_sinlastloan
    })


features = data.apply(get_features, axis=1)
result = pd.concat([data[['id']], features], axis=1)
result.to_csv(r'C:\Users\Ekaterine\Desktop\DE Assessment\contract_features.csv', index=False)
